# Route collections

A route collection is exactly that a group of routes. This could be all your app's routes in one collection, or you could split your web and API routes into seperate collections as you see fit.

Route collection should extend the artbyrab\Atomic\RouteCollectionInterface class.