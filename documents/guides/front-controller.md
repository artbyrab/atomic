# Front controller

In order to use Atomic you need to create a front controller for your app:

* Create a front controller entry script for your app. 
    * Instantiate a new instance of Monolog\Logger
    * Instantiate a new instance of League\Plates\Engine 
    * Create a new request via Laminas\Diactoros\ServerRequestFactory::fromGlobals
    * Instantiate a new instance of League\Route\Router

For a concrete example of how to use Atomic please see artbyrab/atomic-app.