# Controllers

Controllers in Atomic adhere to the idea of a Controller in the MVC design pattern.

Controllers should extend the artbyrab\Atomic\Controller class.