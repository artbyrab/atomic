# Atomic

![Image](files/graphics/atomic-logo-small-padding.png?raw=true)

Atomic is a set of minimal PHP classes that are used to build the Atomic App.

## Installation

Installation is done via composer:

```shell
$ composer require artbyrab/atomic
```

## Usage

In order to use Atomic you typically would:

* Create a front controller entry script for your app. 
    * Instantiate a new instance of Monolog\Logger
    * Instantiate a new instance of League\Plates\Engine 
    * Create a new request via Laminas\Diactoros\ServerRequestFactory::fromGlobals
    * Instantiate a new instance of League\Route\Router

For a concrete example of how to use Atomic please see artbyrab/atomic-app repo.

## Libraries

Atomic makes use of various PHP libraries including:

* league/route
* monolog/monolog
* league/plates
* laminas/laminas-diactoros

## Guides

For more information on Atomic see the documents/guides folder.