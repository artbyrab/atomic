<?php 

namespace artbyrab\Atomic;

use League\Plates\Engine;
use Monolog\Logger;
use Laminas\Diactoros\Response;

/**
 * Controller
 * 
 * A new controller you create should extend this class.
 * 
 * @author artbyrab
 */
abstract class Controller
{
    /**
     * Construct
     * 
     * @param object $template
     * @param object $logger
     */
    public function __construct(Engine $template, Logger $logger)
    {
        $this->template = $template;
        $this->logger = $logger;
    }

    /**
     * Render
     * 
     * This will render a view by wrapping the template render function and 
     * return a PSR reponse.
     * 
     * @param string $view
     * @param array $variables
     * @return object
     */
    protected function render($view, $variables=[])
    {
        $template = $this->template->render($view, $variables);
        
        $response = new Response;
        $response->getBody()->write($template);

        return $response;
    }
}