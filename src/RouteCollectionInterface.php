<?php

namespace artbyrab\Atomic;

use League\Route\Router;
use League\Plates\Engine;
use Monolog\Logger;
use Laminas\Diactoros\ServerRequest;

/**
 * Route collection interface
 * 
 * A new route collection should extend this interface.
 * 
 * @author artbyrab
 */
interface RouteCollectionInterface
{
    /**
     * Add routes
     * 
     * Add one or more routes to a router and then return the router.
     * 
     * @param object $router
     * @param object $request
     * @param object $templates
     * @param object $logger
     * @return object The instance of the router with the new routes added.
     */
    public function addRoutes(Router $router, ServerRequest $request, Engine $template, Logger $logger);
}